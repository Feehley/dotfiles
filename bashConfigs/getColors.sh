#!/bin/bash

getColors(){
	arr=( $(convert +dither "${1}" -colors 20 -unique-colors txt:- | grep -E -o "\#.{6}" | grep -v Image) )

	echo rofi -show run -color-window "${arr[0]}, ${arr[1]}, ${arr[2]}" -color-normal "${arr[3]}, #FFFFFF, ${arr[5]}, ${arr[6]}, #FFFFFF" -color-active "${arr[4]}, #FFFFFF, ${arr[7]}, ${arr[8]}, ${arr[9]}" -color-urgent "${arr[10]}, ${arr[11]}, ${arr[12]}, ${arr[13]}"
}

getColors ${1}
