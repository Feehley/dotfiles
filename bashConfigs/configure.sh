#!/bin/bash

home_dir=$HOME;

mkdir -p ${home_dir}/.vim_runtime/undodir
for conf_file in `ls`; do
	if [[ `file ./${conf_file}` != *"irect"* ]] || [[ "$conf_file" != "${0}" ]]; then
		if [[ "${conf_file}" == "config.rasi" ]]; then
			mkdir -p ${home_dir}/.config/rofi && mv ${conf_file} ${home_dir}/.config/rofi/;
		elif [[ "${conf_file}" == "locker" ]]; then
			mv ${conf_file} /usr/bin/
		else
			mv ./${conf_file} ${home_dir}/.${conf_file};
		fi
	fi
done

echo 'DONE!'
